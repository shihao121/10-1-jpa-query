package com.twuc.webApp.domain;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, String> {
    // TODO
    //
    // 如果需要请在此添加方法。
    //
    // <--start--
//    List<Product> findByProductLine

    List<Product> findByQuantityInStockBetween(Short quantityInStock, Short quantityInStock2);

    List<Product> findByQuantityInStockBetweenOrderByProductCode(Short quantityInStock, short quantityInStock2);

    List<Product> findFirst3ByQuantityInStockBetweenOrderByProductCode(Short quantityInStock, short quantityInStock2);

    Page<Product> findByQuantityInStockBetweenOrderByProductCode(Pageable pageable, Short quantityInStock, short quantityInStock2);

    List<Product> findByProductLineTextDescriptionContains(String keyword);

    // --end-->
}